# Afagh Apps
PRODUCT_PACKAGES += \
    HamedTalk \
    HamedMarket \
    HamedMessaging \
    HamedCloud \
    HamedVPN \
    HamedGSW \
    Silence 

# Afagh priv-apps permissions
PRODUCT_COPY_FILES += \
    vendor/extra/prebuilt/permissions/privapp-permissions-afagh.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-afagh.xml
