#!/system/bin/sh

minor_seclevel="$(getprop ro.sana.build.seclevel.minor)"

if [ "$minor_seclevel" = "1" ]; then
    setprop ro.sana.camera.disabled true
    setprop ro.sana.wifi.disabled true
    setprop ro.sana.bt.disabled true
    setprop ro.sana.nfc.disabled true
    setprop ro.sana.usb.disabled true
    setprop ro.sana.gps.disabled true
fi

if [ "$minor_seclevel" = "2" ]; then
    setprop ro.sana.wifi.disabled true
    setprop ro.sana.bt.disabled true
    setprop ro.sana.nfc.disabled true
    setprop ro.sana.usb.disabled true
    setprop ro.sana.gps.disabled true
fi

if [ "$minor_seclevel" = "3" ]; then
    setprop ro.sana.wifi.disabled true
    setprop ro.sana.bt.disabled true
    setprop ro.sana.nfc.disabled true
    setprop ro.sana.usb.disabled true
    setprop ro.sana.gps.disabled true
fi

setprop ro.sana.iptables.dropall true
setprop ro.sana.iptables.whitelist1 "95.38.94.190"
setprop ro.sana.iptables.whitelist2 "10/8"
setprop ro.sana.iptables.whitelist3 "172.17/16"
