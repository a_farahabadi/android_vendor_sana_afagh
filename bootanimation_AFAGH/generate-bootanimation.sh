#!/bin/bash

PRODUCT_OUT="$1"
WIDTH="$2"
HEIGHT="$3"

OUT="$PRODUCT_OUT/obj/BOOTANIMATION_AFAGH"

RESOLUTION=""$WIDTH"x"$HEIGHT""

mkdir -p "$OUT/bootanimation/part0"
cp "vendor/sana/afagh/bootanimation_AFAGH/0001.png" "$OUT/bootanimation/part0/"

mogrify -resize $RESOLUTION -colors 250 "$OUT/bootanimation/"*"/"*".png"

# Create desc.txt
echo "$WIDTH $HEIGHT" 30 > "$OUT/bootanimation/desc.txt"
cat "vendor/sana/afagh/bootanimation_AFAGH/desc.txt" >> "$OUT/bootanimation/desc.txt"

# Create bootanimation.zip
cd "$OUT/bootanimation"

zip -qr0 "$OUT/bootanimation.zip" .
