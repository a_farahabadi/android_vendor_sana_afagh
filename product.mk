# Project inherits
$(call inherit-product-if-exists, $(LOCAL_PATH)/prebuilt/common.mk)
$(call inherit-product-if-exists, $(LOCAL_PATH)/rootdir/common.mk)

# Overlay
PRODUCT_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(LOCAL_PATH)/overlay

# Props
ifeq ($(SABA_BUILDTYPE),AFAGH_1)
    PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
        lineage.updater.uri="http://update.hamed.ir/afagh/1/api/v1/{device}/{type}/{incr}"
else ifeq ($(SABA_BUILDTYPE),AFAGH_2)
    PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
        lineage.updater.uri="http://update.hamed.ir/afagh/2/api/v1/{device}/{type}/{incr}"
else ifeq ($(SABA_BUILDTYPE),AFAGH_3)
    PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
        lineage.updater.uri="http://update.hamed.ir/afagh/3/api/v1/{device}/{type}/{incr}"
endif


# Custom Packages
PRODUCT_PACKAGES += \
    AfaghPrivilegedExtension \
    KillJelly
