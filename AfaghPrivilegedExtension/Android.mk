LOCAL_PATH:= $(call my-dir)

RELATIVE_PRIVILEGEDEXTENSION_PATH := ../../../../packages/apps/F-DroidPrivilegedExtension/app/src/main

include $(CLEAR_VARS)
include vendor/extra/definitions.mk

LOCAL_PACKAGE_NAME := AfaghPrivilegedExtension
LOCAL_MODULE_TAGS := optional
LOCAL_PRIVILEGED_MODULE := true
LOCAL_SDK_VERSION := current
LOCAL_OVERRIDES_PACKAGES := F-DroidPrivilegedExtension

# Keep IPackageInstallObserver and IPackageDeleteObserver
LOCAL_PROGUARD_ENABLED := disabled
LOCAL_MANIFEST_FILE += $(LOCAL_PATH)/$(RELATIVE_PRIVILEGEDEXTENSION_PATH)/AndroidManifest.xml

LOCAL_SRC_FILES := \
    $(call all-java-files-under, java) \
    $(call all-Iaidl-files-under, $(RELATIVE_PRIVILEGEDEXTENSION_PATH)/aidl) \
    $(call all-java-files-under-except, $(RELATIVE_PRIVILEGEDEXTENSION_PATH)/java, $(call all-java-files-under, java))

LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)/$(RELATIVE_PRIVILEGEDEXTENSION_PATH)/aidl
LOCAL_REQUIRED_MODULES := permissions_org.fdroid.fdroid.privileged.xml

LOCAL_RESOURCE_DIR := \
    $(LOCAL_PATH)/$(RELATIVE_PRIVILEGEDEXTENSION_PATH)/res

include $(BUILD_PACKAGE)
